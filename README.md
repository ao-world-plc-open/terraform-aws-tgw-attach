terraform-AWS-tgw-attach
==========

This module is for creating a Transit Gateway Attachment.

There will be no further development of this module for Terraform 0.11.x

Please use v2.0.0 for Terraform 0.12.x. If you would still like to use this module with Terraform 0.11.x then use v1.2.3

Usage
---

```js
module "vpc_tgw_attach" {
  source               = "git::https://gitlab.com/ao-world-plc-open/terraform-AWS-tgw-attach.git?ref=v1.0.0"
  customer             = "Contoso"
  envname              = "Web"
  envtype              = "QA"
  vpc_id               = module.vpc.vpc_id
  subnet_ids           = [module.vpc.private_subnets]
  private_route_tables = [module.vpc.private_route_tables]
  public_route_tables  = [module.vpc.public_route_tables]
  private_subnets      = ["10.0.11.0/24", "10.0.12.0/24", "10.0.13.0/24"]
  static_routes        = ["10.0.0.0/8"]
  tgw_id               = "tgw-000000000000000000"
  custom_tags = {
    key = "value"
  }
}
```

Variables
---

- `customer` - name of the VPC
- `envname` - The name of the product environment
- `envtype` - The type of deployment environment
- `vpc_id` - The identifier of the VPC the TGW connection is being set up for
- `subnet_ids` - Identifiers of EC2 Subnets provided by the VPC module
- `private_route_tables` - The indentifiers of the private routing tables provided by the VPC module
- `public_route_tables` - The indentifiers of the public routing tables provided by the VPC module
- `private_subnets` - A list of private subnets to associate with routing
- `static_routes` - A list of public subnets to associate with routing
- `tgw_id` - needs to be defined in your params file to the specific region your VPC resides, contact the Infra network team for details or you can see the ID if you browse to Transit Gateways on VPC in the AWS GUI as it is shared to all AO accounts.
- `custom_tags` - A map of tags that will be appended to the tags applied by the module (Name, envtype, envname.). Any tag supplied that matches the name of a tag set by the module will take precedence.
