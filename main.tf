resource "aws_ec2_transit_gateway_vpc_attachment" "vpc_attach" {
  subnet_ids         = flatten([var.subnet_ids])
  transit_gateway_id = var.tgw_id
  vpc_id             = var.vpc_id

  tags = merge(map("Name", "${var.customer}-${var.envname}-${var.envtype}-tgw"), var.custom_tags)
}

#########################################################################################################
#### This Route command for Transit is hashed out as it will not apply until the VPC has been created ###
################# and the Transit attachment has been manually created and accepted #####################
#########################################################################################################

resource "aws_route" "private_transit_all" {
  count                  = length(var.private_subnets)
  route_table_id         = element(flatten(var.private_route_tables), count.index)
  destination_cidr_block = "0.0.0.0/0"
  transit_gateway_id     = var.tgw_id
}

resource "aws_route" "public_transit_mpls" {
  count                  = length(var.static_routes)
  route_table_id         = element(flatten(var.public_route_tables), 0)
  destination_cidr_block = var.static_routes[count.index]
  transit_gateway_id     = var.tgw_id
}
