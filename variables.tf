variable "customer" {}
variable "envname" {}
variable "envtype" {}
variable "vpc_id" {}
variable "tgw_id" {}

variable "static_routes" {
  type = list
}

variable "private_route_tables" {
  type = list
}

variable "public_route_tables" {
  type = list
}

variable "private_subnets" {
  type = list
}

variable "subnet_ids" {
  type = list
}

variable "custom_tags" {
  type        = map
  description = "A map of tags that will be appended to the tags applied by the module (Name, envtype, envname.). Any tag supplied that matches the name of a tag set by the module will take precedence"
  default     = {}
}
